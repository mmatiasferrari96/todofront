import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getTask } from "../actions/index";
import { Flex, Button, Spinner, Text, SimpleGrid, useDisclosure } from "@chakra-ui/react";
import { useAuth } from "../context/authContext";
import TaskCard from "./TaskCard";
import NewTaskModal from "./NewTaskModal";

function Home() {
  const dispatch = useDispatch();
  const task = useSelector((state) => state.default.task);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { user, logout, loading } = useAuth();

  useEffect(() => {
    dispatch(getTask());
  }, [dispatch]);

  const handleLogOut = async () => {
    await logout();
  };

  const handleFormSubmit = () => {
    onClose();
    dispatch(getTask());
  };

  const renderTaskCards = () => {
    return task
      ? task.map((item) => {
          if (item.userId === user.uid) {
            return <TaskCard item={item} key={item.id} />;
          }
          return null;
        })
      : <Text>You still don't have any tasks loaded</Text>;
  };

  if (loading) return <Spinner size="xl" />;

  return (
    <Flex direction="column" margin="10px">
      <Button
        position="absolute"
        top="1rem"
        right="1rem"
        variant="solid"
        colorScheme="red"
        onClick={handleLogOut}
      >
        Log Out
      </Button>
      <Flex justifyContent="center" margin="30px">
        <Text fontSize="4xl" as="b">
          To Do App
        </Text>
      </Flex>
      <SimpleGrid columns={{ sm: 1, md: 2, lg: 3 }} spacing="1rem">
        {renderTaskCards()}
      </SimpleGrid>
      <Flex justifyContent="center">
        <Button
          onClick={onOpen}
          marginBottom="1rem"
          marginTop="30px"
          width="300px"
          size="sm"
          variant="solid"
          colorScheme="teal"
        >
          New Task
        </Button>
      </Flex>
      <NewTaskModal isOpen={isOpen} onClose={onClose} onSubmit={handleFormSubmit} />
    </Flex>
  );
}

export default Home;
