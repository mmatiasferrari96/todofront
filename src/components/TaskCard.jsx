import { Card, CardBody, Heading, Text } from "@chakra-ui/react";

const TaskCard = ({ item }) => {
  return (
    <Card key={item.id} overflow="hidden" variant="outline" flexDirection="column">
      <CardBody>
        <Heading size="md">{item.date}</Heading>
        <Text py="2">{item.description}</Text>
      </CardBody>
    </Card>
  );
};

export default TaskCard;
