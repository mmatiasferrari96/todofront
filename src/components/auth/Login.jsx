import { Box, Center, FormControl, Heading, Input, FormLabel, FormErrorMessage, Button, Link, Text, Alert, AlertIcon } from "@chakra-ui/react";
import { useState } from "react";
import { Link as RouterLink } from "react-router-dom";
import { useAuth } from "../../context/authContext";
import { useNavigate } from "react-router-dom";

export default function Login() {
  const [user, setUser] = useState({
    email: "",
    password: "",
  });
  const { login } = useAuth();
  const navigate = useNavigate();
  const [error, setError] = useState("");

  const handleChange = ({ target: { name, value } }) => {
    setUser({ ...user, [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await login(user.email, user.password);
      navigate("/home");
    } catch (error) {
      setError(error.message);
    }
  };

  return (
    <Center w="100%" h="100vh">
      <Box mx="1" maxW="md" p="9" borderWidth="1px" borderRadius="lg">
        <Heading mb="4" size="lg" textAlign="center">
          Log in
        </Heading>
        {error && (
          <Alert status="error" mb="4">
            <AlertIcon />
            {error}
          </Alert>
        )}
        <form onSubmit={handleSubmit}>
          <FormControl py="2">
            <FormLabel>Email :</FormLabel>
            <Input
              type="email"
              name="email"
              placeholder="Email"
              onChange={handleChange}
            />
            <FormErrorMessage></FormErrorMessage>
          </FormControl>
          <FormControl py="2">
            <FormLabel>Password :</FormLabel>
            <Input
              type="password"
              name="password"
              placeholder="*******"
              onChange={handleChange}
            />
            <FormErrorMessage></FormErrorMessage>
          </FormControl>
          <Button
            mt="4"
            type="submit"
            colorScheme="teal"
            size="md"
            w="full"
            //isLoading={true}
            loadingText="Logging In"
          >
            Log in
          </Button>
        </form>
        <Text>
          Don't have an account?{" "}
          <Link
            as={RouterLink}
            color="teal.800"
            fontWeight="medium"
            textDecor="underline"
            _hover={{ background: "teal.100" }}
            to="/register"
          >
            Register
          </Link>{" "}
          instead!
        </Text>
      </Box>
    </Center>
  );
}