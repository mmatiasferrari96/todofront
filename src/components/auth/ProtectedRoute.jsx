import { Navigate } from "react-router-dom";
import { useAuth } from "../../context/authContext";
import { Spinner } from "@chakra-ui/react";

export function ProtectedRoute({children}) {
    const {user, loading } = useAuth()
    if(loading) return <Spinner size="xl"/>
    if(!user) return <Navigate to='/'/>

    return <>{children}</>
}