import { Button, Flex, Text, useToast } from "@chakra-ui/react";
import { Field, reduxForm } from "redux-form";
import { useAuth } from "../context/authContext";
import { useEffect, useState } from "react";
import { reset } from "redux-form";

const MyForm = ({ handleSubmit, onSubmit, invalid, error }) => {
  const toast = useToast();
  const { user } = useAuth();
  const [userId, setUserId] = useState(null);

  useEffect(() => {
    if (user) {
      setUserId(user.uid);
    }
  }, [user]);

  const submitForm = async (values) => {
    const valuesWithUserId = {
      ...values,
      userId: user.uid,
    };
    onSubmit();

    try {
      const response = await fetch("http://localhost:3000/newTask", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(valuesWithUserId),
      });

      if (response.ok) {
        toast({
          title: "Nueva tarea",
          description: "La tarea se ha creado correctamente.",
          status: "success",
          duration: 3000,
          isClosable: true,
        });
        console.log("Solicitud POST enviada correctamente");
      } else {
        console.log("Error al enviar la solicitud POST");
      }
    } catch (error) {
      console.log("Error al enviar la solicitud POST:", error);
    }

    reset();
  };

  return (
    <form onSubmit={handleSubmit(submitForm)}>
      <Flex direction="column" alignItems="flex-start" marginBottom="1rem">
        <Text fontSize="xl" htmlFor="date" marginBottom="0.5rem">
          Date
        </Text>
        <Field
          name="date"
          component="input"
          type="date"
          borderRadius="md"
          borderWidth="1px"
          padding="0.5rem"
          style={{ width: "100%", height: "40px" }}
        />
        {error && error.date && <Text color="red">{error.date}</Text>}
      </Flex>
      <Flex direction="column" alignItems="flex-start" marginBottom="1rem">
        <Text fontSize="xl" htmlFor="description" marginBottom="0.5rem">
          Description
        </Text>
        <Field
          name="description"
          component="input"
          type="text"
          placeholder="Add the tasks"
          borderRadius="md"
          borderWidth="1px"
          padding="0.5rem"
          style={{ width: "100%", height: "40px" }}
        />
        {error && error.description && (
          <Text color="red">{error.description}</Text>
        )}
      </Flex>
      <Button type="submit" marginTop="1rem" disabled={invalid}>
        Send
      </Button>
    </form>
  );
};

const validate = (values) => {
  const errors = {};

  if (!values.date) {
    errors.date = "Please enter a date";
  }

  if (!values.description) {
    errors.description = "Please enter a description";
  }

  return errors;
};

export default reduxForm({
  form: "myForm",
  validate,
})(MyForm);