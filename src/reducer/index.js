const initialState = {
  task: [],
};

function rootReducer(state = initialState, action) {
  switch (action.type) {
    case "GET_TASK":
      return {
        ...state,
        task: action.payload,
      };
    default:
      return state;
  }
}

export default rootReducer;
