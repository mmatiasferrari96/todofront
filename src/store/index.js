import { createStore, applyMiddleware, combineReducers } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import * as reducers from "../reducer/index";
import { reducer as formReducer } from "redux-form";

const rootReducer = combineReducers({
  ...reducers,
  form: formReducer,
});

export const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
);