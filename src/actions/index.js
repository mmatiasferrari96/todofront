import axios from "axios";

export function getTask() {
  return async function(dispatch) {
    try {
      const response = await axios.get("http://localhost:3000/task");
      //console.log(response.data)
      dispatch({
        type: "GET_TASK",
        payload: response.data,
      });
    } catch (error) {
      console.log("Error al obtener las tareas:", error);
    }
  };
}
