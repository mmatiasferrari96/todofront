// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import {getAuth} from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAtV5jv6Ll1h1mPwN9nD5Z5QxAuAhkW2Sc",
  authDomain: "nodejs-firebase-react.firebaseapp.com",
  projectId: "nodejs-firebase-react",
  storageBucket: "nodejs-firebase-react.appspot.com",
  messagingSenderId: "386745065694",
  appId: "1:386745065694:web:04fa1ae8f94527b4c5624f"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app)